<?php
require_once 'geoip.php';
$base_url = "https://" . $_SERVER['SERVER_NAME'];

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once 'vendor/autoload.php';

$error = '';
$data = [
    'FirstNameLastName' => '',
    'email' => '',
    'password' => '',
    'phone' => '',
];

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // $ip_address = '140.134.26.127';
    //$ip_address = getHostByName(getHostName());

    $frstnameLastname = explode(" ", $_POST['FirstNameLastName']);
    $frstname = $frstnameLastname[0];
    $lastname = $frstnameLastname[1];
    if ($frstnameLastname && sizeof($frstnameLastname) >= 3) {
        $frstname = $frstnameLastname[0];
        $middlename = $frstnameLastname[1];
        $lastname = $frstnameLastname[2];
    } elseif ($frstnameLastname && sizeof($frstnameLastname) == 4) {
        $lastname .= " " .  $frstnameLastname[3];
    } elseif ($frstnameLastname && sizeof($frstnameLastname) == 5) {
        $lastname .= " " .  $frstnameLastname[3] . " " . $frstnameLastname[4];
    }

    $email = $_POST['email'];
    $password = $_POST['password'];
    $phone = $_POST['phone'];

    $device = $_SERVER['HTTP_USER_AGENT'];
    $postal = $_POST['postal'];
    $city = $_POST['city'];
    $citizenship = $_POST['citizenship'];
    $country = $_POST['countryName'];
    $userip = $_POST['userip'];


    if (isset($_POST['clickid'])) {
        $clickid = $_POST['clickid'];
    } else {
        $clickid = 'N/A';
    }

    if (isset($_POST['trackingdomain'])) {
        $td = $_POST['trackingdomain'];
    } else {
        $td = 'N/A';
    }

    function getTitle($url)
    {
        $data = file_get_contents($url);
        $title = preg_match('/<title[^>]*>(.*?)<\/title>/ims', $data, $matches) ? $matches[1] : null;
        return $title;
    }

    $page_title = getTitle($_SERVER['HTTP_REFERER']);

    $subject = "[New Lead] from $country | " . '=?UTF-8?B?' . base64_encode($page_title) . '?=';

    if (isset($middlename)) {
        $middlename_mail = "Middle Name: " . $middlename . "\n\n";
        $lastname_crm = $middlename . '-' . $lastname;
    } else {
        $middlename_mail = '';
        $lastname_crm = $lastname;
    }

    //Time to conversion
    if (isset($_POST['initdate'])) {
        $curr_datetime = new DateTime();
        $finaltime = $curr_datetime->getTimestamp();
        $init_time = $_POST['initdate'];
        $finaltime -= (int)$init_time;
        $finaltime = gmdate("H:i:s", $finaltime);
    }

    //Page URL
    $host  = $_SERVER['HTTP_HOST'];
    $req_uri   = rtrim(dirname(strtok($_SERVER["REQUEST_URI"], '?')), '/\\');
    $page_url = "$host$req_uri";

    $txt = "First Name: " . $frstname . "\n\n"  . $middlename_mail . "Last Name: " . $lastname . "\n\n"  . "Email: " . $email . "\n\n"  . "Password: " . $password . "\n\n" . "Phone: " . $phone . "\n\n" .  "UserIp: " . $ip_address . "\n\n" .  "ClickID: " . $clickid . "\n\n" .  "TrackingDomain: " . $td . "\n\n" .  "Device: " . $device . "\n\n" .  "Country: " . $country . "\n\n" .  "City: " . $city . "\n\n" .  "Zip code: " . $postal . "\n\n" .  "Time to conversion = " . $finaltime . "\n\n" .  "Page URL: " . $page_url;

    // var_dump($txt);
    // return;

    $results_array['ai'] = '2958071';
    $results_array['ci'] = '1';
    $results_array['gi'] = '54';
    $results_array['userip'] = $ip_address;
    $results_array['firstname'] = $frstname;
    $results_array['lastname'] = $lastname_crm;
    $results_array['email'] = $email;
    $results_array['password'] = $password;
    $results_array['phone'] = $phone;
    $results_array['mpc_1'] =  'Lead interested in learning how to invest';
    $results_array['mpc_7'] =  $clickid;
    $results_array['mpc_8'] = $page_title;
    $url = 'https://platform.weleads.co/api/signup/procform';

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
        CURLOPT_HTTPHEADER => array(
            'x-trackbox-username: GIHW',
            'x-trackbox-password: Gihw123!',
            'x-api-key: 2643889w34df345676ssdas323tgc738',
            'Content-Type: application/json'
        ),
        CURLOPT_HEADER =>  false,
        CURLOPT_POST => true,
        CURLOPT_POSTFIELDS => json_encode($results_array),
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_SSL_VERIFYHOST => false
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    $arr = json_decode($response, true);

    if ($arr['status'] === false) {
        $error = $arr['data'];
        $dataErr = json_encode($arr, true);
        // echo $dataErr;
        $output = json_encode(array('status' => 'false', 'message' => $arr['data']));
        die($output);
    } else {
        if ($frstname && $lastname && $email && $password && $phone) {
            try {
                // Create the SMTP Transport
                $transport = (new Swift_SmtpTransport('in-v3.mailjet.com', 587, 'tls'))
                    ->setusername('2e4f3457d06665b9f0cb630a1cd080be')
                    ->setPassword('14c2bde9de727b9625ee3d016e21eb99');

                $mailer = new Swift_Mailer($transport);

                $message = new Swift_Message();

                $message->setSubject(htmlspecialchars($subject));

                $message->setfrom(array('gerry.m@metro-net.co' => 'Gerry'));

                $message->addTo('media@aymob.com', 'Support');
                $message->addCc('ezmarketing111@gmail.com', 'Support');
                $message->addCc('dan@aymob.com', 'Support');
                $message->addCc('aymobanalytics@gmail.com', 'Support');

                $message->setBody($txt);

                if ($mailer->send($message)) {
                }
            } catch (Exception $e) {
                echo 'Message could not be sent. Mailer Error: ', $e->getMessage();
            }
        }
        $dataErr = json_encode($arr, true);
        // echo $dataErr;
        // $output = json_encode(array('status' => 'success', 'done' => 'submission successeded'));
        $output = json_encode(array('status' => true, 'clickid' => $clickid, 'td' => $td));
        die($output);
    }
    curl_close($curl);
}