<?php
$date = new DateTime();
$initdate = $date->getTimestamp();
?>
<?php require_once 'geoip.php'; ?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Trade FOREX with PhoenixMarkets</title>

  <link rel="shortcut icon" href="./assets/img/phoenix-favicon.png" type="image/x-icon">
  <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
  <link rel="stylesheet" href="./assets/css/main.min.css">

  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-M7LW664');</script>
  <!-- End Google Tag Manager -->
</head>

<body class="thankyou-page">
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M7LW664"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->

  <!-- <header class="site-header">
    <div class="container">
      <div class="site-header-wrapper">
        <div class="site-header-logo">
          <img src="./assets/img/logo.png" alt="Trade with tradingcentral">
        </div>
      </div>
    </div>
  </header> -->

  <section class="thankyou">
    <div class="container">
      <div class="row">
        <div class="col-lg-5">
          <div class="thankyou-left">
            <div class="thankyou-content">
              <div class="site-header-logo">
                <a href="https://pro.phoenixmarkets.com/en/trade-with-tradingcentral"><img src="./assets/img/logo.png" alt="Trade with tradingcentral"></a>
              </div>
              <h3 class="big-title"><span class="green">EXPECT A CALL</span></h3>
              <p>We'll send you a text and email confirmation plus </br>a call from our expert partners!</p>
            </div>
            <img src="./assets/img/hand.png" class="hand" alt="hand">
          </div>
        </div>
        <div class="col-lg-7">
          <div class="thankyou-right">
            <div class="site-header-logo">
              <img src="./assets/img/logo.png" alt="Trade with tradingcentral">
            </div>
            <div class="thankyou-content">
              <img src="./assets/img/v.png" alt="check">
              <h3 class="big-title"><span class="gold">Thankyou</span></h3>
              <h3 class="big-title">WE RECEIVED YOUR INFORMATION</h3>

              <h4 class="small-title"><span class="gold">WHAT YOU SHOULD KNOW</span></h4>

              <ul>
                <li data-aos="fade-left" data-aos-delay="100"><svg width="25" height="26" viewBox="0 0 25 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M17.1807 6.87418C16.6958 6.36417 15.9056 6.35774 15.413 6.85984L6.1734 16.3569L2.15687 12.0325C1.67781 11.5132 0.884731 11.4962 0.385493 11.9944C-0.113799 12.4926 -0.130187 13.3174 0.348864 13.8366C0.351035 13.839 0.353151 13.8413 0.355322 13.8436L5.25263 19.1218C5.487 19.3704 5.80733 19.5112 6.14214 19.5127H6.15592C6.48439 19.5116 6.79935 19.3764 7.0329 19.1361L17.1694 8.71262C17.6592 8.20753 17.6642 7.38577 17.1807 6.87418Z" fill="#13CA60" />
                    <path d="M12.7695 19.1227C13.0033 19.3708 13.3226 19.5114 13.6565 19.5136H13.6703C13.9988 19.5125 14.3137 19.3773 14.5473 19.1371L24.6839 8.71356C25.1434 8.17551 25.0964 7.352 24.5791 6.87416C24.1101 6.44096 23.4054 6.43526 22.9299 6.86078L13.6941 16.3579L13.3646 16.0022C12.8856 15.4829 12.0925 15.4658 11.5932 15.964C11.0939 16.4622 11.0775 17.287 11.5565 17.8062C11.5587 17.8086 11.5609 17.8109 11.5631 17.8132L12.7695 19.1227Z" fill="#13CA60" />
                  </svg>
                  <strong>We will not ask you for any sensitive personal details such as date of birth or your bank account information!</strong>
                </li>
                <li data-aos="fade-left" data-aos-delay="200"><svg width="25" height="26" viewBox="0 0 25 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M17.1807 6.87418C16.6958 6.36417 15.9056 6.35774 15.413 6.85984L6.1734 16.3569L2.15687 12.0325C1.67781 11.5132 0.884731 11.4962 0.385493 11.9944C-0.113799 12.4926 -0.130187 13.3174 0.348864 13.8366C0.351035 13.839 0.353151 13.8413 0.355322 13.8436L5.25263 19.1218C5.487 19.3704 5.80733 19.5112 6.14214 19.5127H6.15592C6.48439 19.5116 6.79935 19.3764 7.0329 19.1361L17.1694 8.71262C17.6592 8.20753 17.6642 7.38577 17.1807 6.87418Z" fill="#13CA60" />
                    <path d="M12.7695 19.1227C13.0033 19.3708 13.3226 19.5114 13.6565 19.5136H13.6703C13.9988 19.5125 14.3137 19.3773 14.5473 19.1371L24.6839 8.71356C25.1434 8.17551 25.0964 7.352 24.5791 6.87416C24.1101 6.44096 23.4054 6.43526 22.9299 6.86078L13.6941 16.3579L13.3646 16.0022C12.8856 15.4829 12.0925 15.4658 11.5932 15.964C11.0939 16.4622 11.0775 17.287 11.5565 17.8062C11.5587 17.8086 11.5609 17.8109 11.5631 17.8132L12.7695 19.1227Z" fill="#13CA60" />
                  </svg>
                  Your contact details will not be shared with any third parties without your express permission</li>
                <li data-aos="fade-left" data-aos-delay="300"><svg width="25" height="26" viewBox="0 0 25 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M17.1807 6.87418C16.6958 6.36417 15.9056 6.35774 15.413 6.85984L6.1734 16.3569L2.15687 12.0325C1.67781 11.5132 0.884731 11.4962 0.385493 11.9944C-0.113799 12.4926 -0.130187 13.3174 0.348864 13.8366C0.351035 13.839 0.353151 13.8413 0.355322 13.8436L5.25263 19.1218C5.487 19.3704 5.80733 19.5112 6.14214 19.5127H6.15592C6.48439 19.5116 6.79935 19.3764 7.0329 19.1361L17.1694 8.71262C17.6592 8.20753 17.6642 7.38577 17.1807 6.87418Z" fill="#13CA60" />
                    <path d="M12.7695 19.1227C13.0033 19.3708 13.3226 19.5114 13.6565 19.5136H13.6703C13.9988 19.5125 14.3137 19.3773 14.5473 19.1371L24.6839 8.71356C25.1434 8.17551 25.0964 7.352 24.5791 6.87416C24.1101 6.44096 23.4054 6.43526 22.9299 6.86078L13.6941 16.3579L13.3646 16.0022C12.8856 15.4829 12.0925 15.4658 11.5932 15.964C11.0939 16.4622 11.0775 17.287 11.5565 17.8062C11.5587 17.8086 11.5609 17.8109 11.5631 17.8132L12.7695 19.1227Z" fill="#13CA60" />
                  </svg>
                  All advisors on our panel are recognized and regulated by the Financial Conduct Authority</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
  <script>
    AOS.init();
  </script>
</body>

</html>