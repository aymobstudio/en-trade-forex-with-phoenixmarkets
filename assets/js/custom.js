let $ = jQuery;

function stickyHeader() {
  const header = document.querySelector("#sticky-btn-wrapper");
  const scrollUp = "sticky";
  const scrollDown = "sticky-up";
  let lastScroll = 0;

  window.addEventListener("scroll", () => {
    const currentScroll = window.pageYOffset;
    if (currentScroll <= 150) {
      header.classList.remove(scrollUp);
      header.classList.remove(scrollDown);
      return;
    }

    if (currentScroll > lastScroll && !header.classList.contains(scrollDown)) {
      // down
      header.classList.remove(scrollUp);
      header.classList.add(scrollDown);
    } else if (
      currentScroll < lastScroll &&
      header.classList.contains(scrollDown)
    ) {
      // up
      header.classList.remove(scrollDown);
      header.classList.add(scrollUp);
    }
    lastScroll = currentScroll;
  });
}

stickyHeader();

// Master form STARTS

const regCheckBox = document.getElementById("custom-checkbox-input");
const regActionBtn = document.getElementById("button-trading");

let validation = {
  name: false,
  email: false,
  pass: false,
  phone: false,
  checkbox: true,
}

$("#interest-form").submit(function (e) {

  e.preventDefault();

  let form = $(this);
  let url = $(form).attr('action');

  $.ajax({
    type: "POST",
    url: url,
    dataType: "json",
    data: form.serialize(),
    beforeSend: function (xhr) {
      $('#button-trading').attr('disabled', true).html("Processing...");
    },
    success: function (res) {
      $('#button-trading').attr('disabled', false).html(`
            Start Trading
        `);
      if (res.status == 'false') {
        $('#error-box').fadeIn().html(res.message);
        setTimeout(function () {
          $('#error-box').fadeOut().html();
        }, 5000);
      } else {
        location.href = `thankyou.php?clickid=${res.clickid}&td=${res.td}`;
        console.log('Success');
      }
    }
  });
});
const phoneInputField = document.querySelector("#phone");
const phoneInput = window.intlTelInput(phoneInputField, {
  initialCountry: "il",
  geoIpLookup: function (success) {
    // Get your api-key at https://ipdata.co/
    fetch("https://api.ipdata.co/?api-key=fb6483095ab6d6ef96d67904a2d8fe9e05c462b3d7063c54f5024f89")
      .then(function (response) {
        if (!response.ok) return success("");
        return response.json();
      })
      .then(function (ipdata) {
        success(ipdata.country_code);
      });
  },
  utilsScript: "https://intl-tel-input.com/node_modules/intl-tel-input/build/js/utils.js?1613236686837", // just for formatting/placeholders etc
});

regActionBtn.addEventListener('click', function () {
  let code = phoneInput.getNumber();
  phoneInputField.value = code;
});

// phone number validation
$('#phone').first().keyup(function () {
  let phone = this.value;
  if (phone.length < 5) {
    document.getElementById("phone-message").innerHTML = "This field is required.";
    validation.phone = false;
    checkValidations();
  } else {
    document.getElementById("phone-message").innerHTML = " ";
    validation.phone = true;
    checkValidations();
    return true;
  }
});

const checkName = function () {
  let fname = document.getElementById("fname").value;
  let regExp = fname.split(" ").length <= 2 ? new RegExp("^([a-zA-Z]{2,}\\s[a-zA-Z]{2,})") : new RegExp("^([a-zA-Z]{2,}\\s[a-zA-Z]{1,}\\s[a-zA-Z]{2,})");
  if ((fname.length > 32) || (fname.length < 2) || (fname.search(/[0-9]/) > 0) || (!regExp.test(fname) && fname) || (fname.split(" ").length > 3)) {
    document.getElementById("fname-message").innerHTML = "The full name field is not valid.";
    validation.name = false;
    checkValidations();
    return false;
  }
  document.getElementById("fname-message").innerHTML = " ";
  validation.name = true;
  checkValidations();
  return true;
}

const checkEmail = function () {
  let email = document.getElementById("email").value;
  const emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  if ((email.length < 2) || (!emailReg.test(email))) {
    document.getElementById("email-message").innerHTML = "Please enter a valid email.";
    validation.email = false;
    checkValidations();
  } else {
    document.getElementById("email-message").innerHTML = " ";
    validation.email = true;
    checkValidations();
    return true;
  }
}

const checkPassword = function () {
  let pw = document.getElementById("password").value;
  if ((pw.length < 8) || (pw.search(/[a-z]/) < 0) || (pw.search(/[A-Z]/) < 0) || (pw.search(/[0-9]/) < 0) || (pw.length > 15)) {
    document.getElementById("password-message").innerHTML = "The password length must be 8-15 characters long.";
    validation.pass = false;
    checkValidations();
    return false;
  }
  document.getElementById("password-message").innerHTML = " ";
  validation.pass = true;
  checkValidations();
  return true;
}

const checkValidations = function () {
  Object.values(validation).some(val => val === false) ?
    regActionBtn.setAttribute("disabled", "disabled") :
    regActionBtn.removeAttribute("disabled");
}

regCheckBox.addEventListener('click', function () {
  validation.checkbox = !validation.checkbox;
  checkValidations();
});

(function () {
  if (!(validation.name && validation.email && validation.pass && validation.phone && validation.checkbox)) regActionBtn.setAttribute("disabled", "disabled");
})();

// Master form ENDS


document.addEventListener("DOMContentLoaded", function() {
  const modalTrigger = document.querySelectorAll("[data-modal]"),
    modal = document.querySelector(".help-window"),
    modalCloseBtn = document.querySelector("[data-close]");
  
    if (modal) {
      modalTrigger.forEach((btn) => {
        btn.addEventListener("click", openModal);
      });
      function closeModal() {
        modal.classList.add("hide");
        modal.classList.remove("show");
        document.body.style.overflow = "";
      }
      
      function openModal() {
        modal.classList.add("show");
        modal.classList.remove("hide");
        document.body.style.overflow = "hidden";
      }
      
      modalCloseBtn.addEventListener("click", closeModal);
      modal.addEventListener("click", (e) => {
        if (e.target === modal) {
          closeModal();
        }
      });
      
      document.addEventListener("keydown", (e) => {
        if (e.code === "Escape" && modal.classList.contains("show")) {
          closeModal();
        }
      });
    }
});

// Help form STARTS


const helpCheckBox = document.getElementById("help-checkbox-input");
const helpActionBtn = document.getElementById("help-form-btn");

let validationHelp = {
  name: false,
  email: false,
  phone: false,
  checkbox: true,
}

$("#help-form").submit(function (e) {

  e.preventDefault(); // avoid to execute the actual submit of the form.

  let form = $(this);
  let url = $(form).attr('action');

  $.ajax({
    type: "POST",
    url: url,
    dataType: "json",
    data: form.serialize(),
    beforeSend: function (xhr) {
      $('#help-form-btn').attr('disabled', true).html("Processing...");
    },
    success: function (res) {
      // console.log('RES: ', res);
      $('#help-form-btn').attr('disabled', false).html(`
            BOOK A CALL
        `);
      if (res.status == 'false') {
        $('#help-error-box').fadeIn().html(res.message);
        setTimeout(function () {
          $('#help-error-box').fadeOut().html();
        }, 5000);
      } else {
        location.href = `thankyou.php?clickid=${res.clickid}&td=${res.td}`;
        console.log('Success');
      }
    }
  });
});
const phoneInputFieldHelp = document.querySelector("#help-phone");
const phoneInputHelp = window.intlTelInput(phoneInputFieldHelp, {
  initialCountry: "il",
  geoIpLookup: function (success) {
    // Get your api-key at https://ipdata.co/
    fetch("https://api.ipdata.co/?api-key=fb6483095ab6d6ef96d67904a2d8fe9e05c462b3d7063c54f5024f89")
      .then(function (response) {
        if (!response.ok) return success("");
        return response.json();
      })
      .then(function (ipdata) {
        success(ipdata.country_code);
      });
  },
  utilsScript: "https://intl-tel-input.com/node_modules/intl-tel-input/build/js/utils.js?1613236686837", // just for formatting/placeholders etc
});

helpActionBtn.addEventListener('click', function () {
  let code = phoneInputHelp.getNumber();
  phoneInputFieldHelp.value = code;
});

// phone number validation
$('#help-phone').first().keyup(function () {
  let phone = this.value;
  if (phone.length < 5) {
    document.getElementById("help-phone-message").innerHTML = "This field is required.";
    validationHelp.phone = false;
    checkValidationsHelp();
  } else {
    document.getElementById("help-phone-message").innerHTML = " ";
    validationHelp.phone = true;
    checkValidationsHelp();
    return true;
  }
});

const checkNameHelp = function () {
  let fname = document.getElementById("help-fname").value;
  let regExp = fname.split(" ").length <= 2 ? new RegExp("^([a-zA-Z]{2,}\\s[a-zA-Z]{2,})") : new RegExp("^([a-zA-Z]{2,}\\s[a-zA-Z]{1,}\\s[a-zA-Z]{2,})");
  if ((fname.length > 32) || (fname.length < 2) || (fname.search(/[0-9]/) > 0) || (!regExp.test(fname) && fname) || (fname.split(" ").length > 3)) {
    document.getElementById("help-fname-message").innerHTML = "The full name field is not valid.";
    validationHelp.name = false;
    checkValidationsHelp();
    return false;
  }
  document.getElementById("help-fname-message").innerHTML = " ";
  validationHelp.name = true;
  checkValidationsHelp();
  return true;
}

const checkEmailHelp = function () {
  let email = document.getElementById("help-email").value;
  const emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  if ((email.length < 2) || (!emailReg.test(email))) {
    document.getElementById("help-email-message").innerHTML = "This field is required.Please enter a valid email.";
    validationHelp.email = false;
    checkValidationsHelp();
  } else {
    document.getElementById("help-email-message").innerHTML = " ";
    validationHelp.email = true;
    checkValidationsHelp();
    return true;
  }
}

const checkValidationsHelp = function () {
  Object.values(validationHelp).some(val => val === false) ?
    helpActionBtn.setAttribute("disabled", "disabled") :
    helpActionBtn.removeAttribute("disabled");
}

helpCheckBox.addEventListener('click', function () {
  validationHelp.checkbox = !validationHelp.checkbox;
  checkValidationsHelp();
});

(function () {
  if (!(validationHelp.name && validationHelp.email && validationHelp.phone && validationHelp.checkbox)) helpActionBtn.setAttribute("disabled", "disabled");
})();

// Help form ENDS